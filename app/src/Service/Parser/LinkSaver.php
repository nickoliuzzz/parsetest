<?php

namespace App\Service\Parser;

use App\Entity\AbstractNews;
use App\Entity\HtmlNews;
use App\Entity\LinkNews;

class LinkSaver implements SiteParserInterface
{
    public function isSupported(string $type): bool
    {
        return false;
    }

    public function parseNews(string $href, string $title): LinkNews
    {
        return new LinkNews($title, $href);
    }
}