<?php

namespace App\Service\Parser;

use App\Entity\AbstractNews;

interface SiteParserInterface
{
    public function isSupported(string $type): bool;

    public function parseNews(string $href, string $title): AbstractNews;
}