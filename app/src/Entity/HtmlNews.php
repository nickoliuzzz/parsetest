<?php

namespace App\Entity;

use App\Repository\AbstractNewsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AbstractNewsRepository::class)
 */
class HtmlNews extends AbstractNews
{
    /**
     * @ORM\Column(type="text")
     */
    private string $content = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $photo;

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function addContent(string $content): self
    {
        $this->content .= $content;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }
}
