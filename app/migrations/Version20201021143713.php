<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201021143713 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE abstract_news (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(200) NOT NULL, link LONGTEXT NOT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE html_news (id INT NOT NULL, content LONGTEXT NOT NULL, photo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE link_news (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE html_news ADD CONSTRAINT FK_3A2B542ABF396750 FOREIGN KEY (id) REFERENCES abstract_news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE link_news ADD CONSTRAINT FK_AC940BF9BF396750 FOREIGN KEY (id) REFERENCES abstract_news (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE html_news DROP FOREIGN KEY FK_3A2B542ABF396750');
        $this->addSql('ALTER TABLE link_news DROP FOREIGN KEY FK_AC940BF9BF396750');
        $this->addSql('DROP TABLE abstract_news');
        $this->addSql('DROP TABLE html_news');
        $this->addSql('DROP TABLE link_news');
    }
}
