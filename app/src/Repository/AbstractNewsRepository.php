<?php

namespace App\Repository;

use App\Entity\AbstractNews;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AbstractNews|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractNews|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractNews[]    findAll()
 * @method AbstractNews[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractNewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractNews::class);
    }
}
