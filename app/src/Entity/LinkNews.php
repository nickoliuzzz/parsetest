<?php

namespace App\Entity;

use App\Repository\AbstractNewsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AbstractNewsRepository::class)
 */
class LinkNews extends AbstractNews
{
}
