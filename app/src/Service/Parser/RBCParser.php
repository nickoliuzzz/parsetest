<?php

namespace App\Service\Parser;

use App\Entity\HtmlNews;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RBCParser implements SiteParserInterface
{
    const SITE_PATTERN = 'www.rbc.ru';
    const UPLOAD_DIR = '/uploads/photo/';
    const MAIN_IMAGE = 'img[class="article__main-image__image"]';
    const CONTENT_CLASS = '.article__content';

    private HttpClientInterface $client;
    private string $uploadsDirectory;

    public function __construct(HttpClientInterface $client, string $uploadsDirectory)
    {
        $this->client = $client;
        $this->uploadsDirectory = $uploadsDirectory;
    }

    public function isSupported(string $type): bool
    {
        return self::SITE_PATTERN === $type;
    }

    public function parseNews(string $href, string $title): HtmlNews
    {
        $news = new HtmlNews($title, $href);
        $response = $this->client->request(Request::METHOD_GET, $href);

        $crawler = new Crawler($response->getContent());
        $newsDom = $crawler->filter(self::CONTENT_CLASS);

        $news->setPhoto($this->saveImage($newsDom));

        foreach ($newsDom->filter('p') as $node) {
            $news->addContent($this->wrapContentIntoTAg($node->textContent));
        }

        return $news;
    }

    private function wrapContentIntoTAg(string $content, string $tag = 'p'): string
    {
        return "<$tag>".$content."</$tag>";
    }

    private function saveImage(Crawler $crawler): ?string
    {
        if ($node = $crawler->filter(self::MAIN_IMAGE)->getNode(0))
            {
            $photoUrl = $node->attributes->getNamedItem('src')->nodeValue;
            $content = file_get_contents($photoUrl);
            $fileName = hash('sha256', $content);

            $fp = fopen($this->getFilePath($fileName), "w");

            fwrite($fp, $content);
            fclose($fp);
        }

        return $fileName ?? null;
    }

    private function getFilePath(string $fileName): string
    {
        return $this->uploadsDirectory.self::UPLOAD_DIR.$fileName;
    }
}