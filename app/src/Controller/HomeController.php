<?php

namespace App\Controller;

use App\Entity\AbstractNews;
use App\Repository\AbstractNewsRepository;
use App\Service\NewsFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/news", name="news")
     */
    public function newsList(AbstractNewsRepository $repository)
    {
        return $this->render('news/list.html.twig', [
            'news' => $repository->findAll(),
        ]);
    }

    /**
     * @Route("/news/{news}", name="news_get")
     */
    public function news(AbstractNews $news)
    {
        return $this->render('news/news.html.twig', [
            'news' => $news,
        ]);
    }
}
