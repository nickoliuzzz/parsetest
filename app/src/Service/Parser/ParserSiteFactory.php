<?php

namespace App\Service\Parser;

class ParserSiteFactory
{
    /** @var iterable|SiteParserInterface[]  */
    private iterable $siteParsers;

    private LinkSaver $linkSaver;

    public function __construct(iterable $siteParsers, LinkSaver $linkSaver)
    {

        $this->siteParsers = $siteParsers;
        $this->linkSaver = $linkSaver;
    }

    public function getSiteParser(string $type): SiteParserInterface
    {
        foreach ($this->siteParsers as $siteParser) {
            if ($siteParser->isSupported($type)) {
                return $siteParser;
            }
        }

        return $this->linkSaver;
    }
}