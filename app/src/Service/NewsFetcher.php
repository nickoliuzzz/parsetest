<?php

namespace App\Service;

use App\Service\Parser\ParserSiteFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NewsFetcher
{
    private const URL = 'https://www.rbc.ru/v10/ajax/get-news-feed/project/rbcnews/lastDate/%s/limit/%d';
    const LIMIT = 15;
    const TITLE_LENGTH = 200;

    private HttpClientInterface $client;
    private ParserSiteFactory $factory;
    private EntityManagerInterface $em;

    public function __construct(HttpClientInterface $client, ParserSiteFactory $factory, EntityManagerInterface $em)
    {
        $this->client = $client;
        $this->factory = $factory;
        $this->em = $em;
    }

    public function fetchNews()
    {
        $response = $this->client->request(Request::METHOD_GET, $this->getUrl());

        foreach (json_decode($response->getContent())->items as $item) {
            $crawler = new Crawler($item->html);

            $href = $crawler->filter('a')->attr('href');
            $title = trim($crawler->filter('span')->text());
            $siteType = explode('/', $href)[2];
            $siteParser = $this->factory->getSiteParser($siteType);
            $this->em->persist($siteParser->parseNews($href, $title));
        }

        $this->em->flush();
    }

    private function getUrl(): string
    {
        return sprintf(self::URL, (new \DateTime())->getTimestamp(), self::LIMIT);
    }
}