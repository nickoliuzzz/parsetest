<?php

namespace App\Command;

use App\Service\NewsFetcher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FetchNewsCommand extends Command
{
    protected static $defaultName = 'app:fetch-news';
    private NewsFetcher $newsFetcher;

    public function __construct(string $name = null, NewsFetcher $newsFetcher)
    {
        $this->newsFetcher = $newsFetcher;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Fetch News from rbc.ru')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->newsFetcher->fetchNews();

        $io->success(sprintf('Ok. Last %d news were gotten.', NewsFetcher::LIMIT));

        return 0;
    }
}
